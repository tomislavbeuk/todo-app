

var data = (localStorage.getItem('todoList')) ? JSON.parse(localStorage.getItem('todoList')) : {todo:[], completed:[]};

// Remove and complete icons in svg format
var removeSvg = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><rect class="noFill" width="22" height="22"/><g><g><path class="fill" d="M16.1,3.6h-1.9V3.3c0-1.3-1-2.3-2.3-2.3h-1.7C8.9,1,7.8,2,7.8,3.3v0.2H5.9c-1.3,0-2.3,1-2.3,2.3v1.3c0,0.5,0.4,0.9,0.9,1v10.5c0,1.3,1,2.3,2.3,2.3h8.5c1.3,0,2.3-1,2.3-2.3V8.2c0.5-0.1,0.9-0.5,0.9-1V5.9C18.4,4.6,17.4,3.6,16.1,3.6z M9.1,3.3c0-0.6,0.5-1.1,1.1-1.1h1.7c0.6,0,1.1,0.5,1.1,1.1v0.2H9.1V3.3z M16.3,18.7c0,0.6-0.5,1.1-1.1,1.1H6.7c-0.6,0-1.1-0.5-1.1-1.1V8.2h10.6L16.3,18.7L16.3,18.7z M17.2,7H4.8V5.9c0-0.6,0.5-1.1,1.1-1.1h10.2c0.6,0,1.1,0.5,1.1,1.1V7z"/></g><g><g><path class="fill" d="M11,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6s0.6,0.3,0.6,0.6v6.8C11.6,17.7,11.4,18,11,18z"/></g><g><path class="fill" d="M8,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8C7.4,10.2,7.7,10,8,10c0.4,0,0.6,0.3,0.6,0.6v6.8C8.7,17.7,8.4,18,8,18z"/></g><g><path class="fill" d="M14,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C14.6,17.7,14.3,18,14,18z"/></g></g></g></svg>`;
var completeSvg = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><rect class="noFill" width="22" height="22"/><g><path class="fill" d="M9.7,14.4L9.7,14.4c-0.2,0-0.4-0.1-0.5-0.2l-2.7-2.7c-0.3-0.3-0.3-0.8,0-1.1s0.8-0.3,1.1,0l2.1,2.1l4.8-4.8c0.3-0.3,0.8-0.3,1.1,0s0.3,0.8,0,1.1l-5.3,5.3C10.1,14.3,9.9,14.4,9.7,14.4z"/></g></svg>`;

// populate lists if data exist 
if(data.todo.length) {
    data.todo.forEach(function(listITemValue) {
        createTodoItem(listITemValue, '#todo');
    });
}

if(data.completed.length) {
    data.completed.forEach(function(listITemValue) {
        createTodoItem(listITemValue, '#completed');
    });
}

function dataObjectUpdate(dataObj) {
    localStorage.setItem('todoList', JSON.stringify(dataObj));
}

// Add item to list
function addItem() {
    InputValue = document.querySelector('#item');
    if(InputValue.value.trim()) {
        // add value to data object and create list item
        data.todo.push(InputValue.value);

        createTodoItem(InputValue.value, '#todo');

        InputValue.value = '';

        dataObjectUpdate(data);
    } 
}

// Delete item from list and array
function deleteItem() {
    var listItem = this.parentNode.parentNode;
    var todoList = listItem.parentNode;
    var listItemValue = listItem.textContent
    var targetArr = (todoList.classList.contains('completed')) ? 'completed' : 'todo'; 

    data[targetArr].splice(data[targetArr].indexOf(listItemValue), 1);

    todoList.removeChild(listItem);

    dataObjectUpdate(data);
}

// Toggle list items
function completeItem() {
    var listItem = this.parentNode.parentNode;
    var todoList = listItem.parentNode;
    var listItemValue = listItem.textContent
    var targetTodoList = '';

    // Check which list and array should be updated
    if(!todoList.classList.contains('completed')) {
        targetTodoList = document.querySelector('#completed');
        data.todo.splice(data.todo.indexOf(listItemValue), 1);
        data.completed.push(listItemValue);
    }
    else {
        targetTodoList = document.querySelector('#todo');
        data.completed.splice(data.completed.indexOf(listItemValue), 1);
        data.todo.push(listItemValue);
    }

    dataObjectUpdate(data);

    targetTodoList.insertBefore(listItem, targetTodoList.childNodes[0]);
}

// Create list item
function createTodoItem(text, listParam) {
    var list = document.querySelector(listParam);

    // create all html elements
    var listItem = document.createElement('li');
    listItem.classList.add('todo-item');
    listItem.textContent = text;

    var buttonsDiv = document.createElement('div');
    buttonsDiv.classList.add('todo-buttons');

    var removeTodoButton = document.createElement('button');
    removeTodoButton.classList.add('todo-btn', 'remove');
    removeTodoButton.innerHTML = removeSvg;
    removeTodoButton.addEventListener('click', deleteItem);

    var completeTodoButton = document.createElement('button');
    completeTodoButton.classList.add('todo-btn', 'complete');
    completeTodoButton.innerHTML = completeSvg;
    completeTodoButton.addEventListener('click', completeItem);

    // Append created list item to list
    buttonsDiv.appendChild(removeTodoButton);
    buttonsDiv.appendChild(completeTodoButton);
    listItem.appendChild(buttonsDiv);
    list.insertBefore(listItem, list.childNodes[0]);
}

document.querySelector('#add').addEventListener('click', addItem);
document.querySelector('#item').addEventListener('keydown', function(event) {
    InputValue = document.querySelector('#item');

    if(event.keyCode == 13 && InputValue.value.trim()) {
        addItem();
    }
   
});
